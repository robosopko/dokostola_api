const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    enabled: false,
    content: ["./**/*.twig"]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'light-blue': colors.lightBlue,
        cyan: colors.cyan,
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
