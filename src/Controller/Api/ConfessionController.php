<?php

namespace App\Controller\Api;

use App\Entity\Api\ChurchBasicInfo;
use App\Entity\Api\Confession;
use App\Entity\Api\DateSettings;
use App\Service\Api\CalendarService;
use App\Service\Api\ChurchService;
use App\Service\Api\ConfessionService;
use App\Service\Api\ImageService;
use App\Service\Api\TownService;
use App\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ConfessionController extends AbstractController
{
    private $churchService;
    private $confessionService;
    private $imageService;
    private $calendarService;
    private $townService;

    public function __construct(
        ChurchService $churchService,
        ConfessionService $confessionService,
        ImageService $imageService,
        CalendarService $calendarService,
        TownService $townService
    )
    {
        $this->churchService = $churchService;
        $this->confessionService = $confessionService;
        $this->imageService = $imageService;
        $this->calendarService = $calendarService;
        $this->townService = $townService;
    }

    private function getConfessionsInChurch(array $churchIds, array $churches, string $date): array
    {
        if (count($churches) == 0) {
            $churches = $this->churchService->getChurches($churchIds);
        }
        
        $churchBasicInfoArray = $this->churchService->getChurchBasicInfo($churchIds);

        foreach ($churchBasicInfoArray as $churchId => $churchBasicInfo) {
            $church = $churches[$churchId];
            $churches[$churchId] = Utils::mergeObjects($church, $churchBasicInfo);
        }

        $churchImagesArray = $this->imageService->getImages($churchIds);

        foreach ($churchImagesArray as $image) {
            array_push($churches[$image->parentId]->images, $image);
        }

        $dateSettings = $this->calendarService->getDateSettings($date);

        $regularConfessions = $this->confessionService->getRegularConfessions($churchIds, DateSettings::dateSettingsToDayOfWeek($dateSettings), false);
        $regularConfessionsInSeason = $this->confessionService->getRegularConfessions($churchIds, DateSettings::dateSettingsToDayOfWeek($dateSettings), true);
        $extraordinaryConfessions = $this->confessionService->getExtraordinaryConfessions($churchIds, $date);

        $confessions = [];
        /**
         * @var ChurchBasicInfo $church
         */
        foreach ($churches as $churchId => $church) {
            /**
             * @var Confession[] $churchConfessions
             */
            $churchConfessions = [];
            /**
             * @var Confession[] $churchExtraordinaryConfessions
             */
            $churchExtraordinaryConfessions = isset($extraordinaryConfessions[$churchId]) ? $extraordinaryConfessions[$churchId] : [];

            $noConfessionToday = count(
                array_filter($churchExtraordinaryConfessions, function($confession) {
                    return $confession->noConfessionToday == true;
                })
            ) > 0;

            if ($noConfessionToday) {
                $confessions[$churchId] = [];
                continue;
            }

            $churchConfessions = array_merge($churchConfessions, $churchExtraordinaryConfessions);

            $overrideRegular = count(
                array_filter($churchExtraordinaryConfessions, function($confession) {
                    return $confession->overrideRegular == true;
                })
            ) > 0;

            if ($overrideRegular || CalendarService::isConfessionSpecialDate($dateSettings->date)) {
                $confessions[$churchId] = $churchConfessions;
                continue;
            }

            if (CalendarService::isInSeason($dateSettings->date, $church->seasonConfessionsFrom, $church->seasonConfessionsTo)) {
                $churchConfessions = isset($regularConfessionsInSeason[$churchId]) ? array_merge($churchConfessions, $regularConfessionsInSeason[$churchId]) : $churchConfessions;
            } else {
                $churchConfessions = isset($regularConfessions[$churchId]) ? array_merge($churchConfessions, $regularConfessions[$churchId]) : $churchConfessions;
            };

            $confessions[$churchId] = $churchConfessions;
        }

        return [
            'churches' => $churches,
            'confessions' => $confessions
        ];
    }

    private function getConfessionsInTown(array $townIds, string $date): array
    {
        if (count($townIds) > 0 && !is_numeric($townIds[0])) {
            $townIds = $this->townService->getTownIdsByAliases($townIds);
        }

        $townIds = $this->townService->expandTownIds($townIds);
        $churches = $this->churchService->getChurchesInTown($townIds);
        $churchIds = array_keys($churches);

        return $this->getConfessionsInChurch($churchIds, $churches, $date);
    }

    /**
     * @Route("/confessions", name="app_confessions")
     */
    public function confessionsAction(Request $request): JsonResponse
    {
        $townIdParam = $request->query->get('townId', '');
        $churchIdParam = $request->query->get('churchId', '');

        $townIds = $townIdParam ? explode(',', $townIdParam) : [];
        $churchIds = $churchIdParam ? explode(',', $churchIdParam) : [];
        $date = $request->query->get('date', date('Y-m-d'));

        if (count($churchIds) > 0) {
            $confessionsInChurchData = $this->getConfessionsInChurch($churchIds, [], $date);
            return $this->json($confessionsInChurchData);
        } else {
            $confessionsInTownData = $this->getConfessionsInTown($townIds, $date);
            return $this->json($confessionsInTownData);
        }
    }

    /**
     * @Route("/confessions-in-town", name="app_confessions_in_town")
     */
    public function confessionsInTownAction(Request $request): JsonResponse
    {
        $townIds = explode(',', $request->query->get('townId', ''));
        $date = $request->query->get('date', date('Y-m-d'));

        $confessionsInTownData = $this->getConfessionsInTown($townIds, $date);

        return $this->json($confessionsInTownData);
    }

    /**
     * @Route("/confession-times-in-town", name="app_confession_times_in_town")
     */
    public function confessionTimesInTownAction(Request $request): JsonResponse
    {
        $townIds = explode(',', $request->query->get('townId', ''));
        $date = $request->query->get('date', date('Y-m-d'));

        $confessionsInTownData = $this->getConfessionsInTown($townIds, $date);
        $confessions = array_merge(...array_values($confessionsInTownData['confessions']));
        $times = array_map(function (Confession $confession) {
            return $confession->time;
        }, $confessions);
        $uniqueTimes = array_unique($times);
        sort($uniqueTimes);

        return $this->json($uniqueTimes);
    }

    /**
     * @Route("/confessions-in-church", name="app_confessions_in_church")
     */
    public function confessionsInChurchAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');
        $regular = $this->confessionService->getChurchRegularConfessions($id);
        $seasonal = $this->confessionService->getChurchSeasonalConfessions($id);
        $extraordinary = $this->confessionService->getChurchExtraordinaryConfessions($id);

        return $this->json([
            'regular' => $regular,
            'seasonal' => $seasonal,
            'extraordinary' => $extraordinary
        ]);
    }
}
