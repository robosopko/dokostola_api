<?php

namespace App\Controller\Api;

use App\Entity\Api\ChurchBasicInfo;
use App\Entity\Api\DateSettings;
use App\Entity\Api\Mass;
use App\Service\Api\CalendarService;
use App\Service\Api\ChurchService;
use App\Service\Api\ImageService;
use App\Service\Api\MassService;
use App\Service\CommonService;
use App\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class MassController extends AbstractController
{
    private $commonService;
    private $churchService;
    private $massService;
    private $imageService;
    private $calendarService;

    public function __construct(
        CommonService $commonService,
        ChurchService $churchService,
        MassService $massService,
        ImageService $imageService,
        CalendarService $calendarService
    )
    {
        $this->commonService = $commonService;
        $this->churchService = $churchService;
        $this->massService = $massService;
        $this->imageService = $imageService;
        $this->calendarService = $calendarService;
    }

    /**
     * @Route("/masses", name="app_masses")
     */
    public function massesAction(Request $request): JsonResponse
    {
        $townIdParam = $request->query->get('townId', '');
        $churchIdParam = $request->query->get('churchId', '');

        $townIds = $townIdParam ? explode(',', $townIdParam) : [];
        $churchIds = $churchIdParam ? explode(',', $churchIdParam) : [];
        $date = $request->query->get('date', date('Y-m-d'));

        if (count($churchIds) > 0) {
            $massesInChurchData = $this->commonService->getMassesInChurch($churchIds, [], $date);    
            return $this->json($massesInChurchData);
        } else {
            $massesInTownData = $this->commonService->getMassesInTown($townIds, $date);
            return $this->json($massesInTownData);
        }
    }

    /**
     * @Route("/masses-in-town", name="app_masses_in_town")
     */
    public function massesInTownAction(Request $request): JsonResponse
    {
        $townIds = explode(',', $request->query->get('townId', ''));
        $date = $request->query->get('date', date('Y-m-d'));

        $massesInTownData = $this->commonService->getMassesInTown($townIds, $date);

        return $this->json($massesInTownData);
    }

    /**
     * @Route("/mass-times-in-town", name="app_mass_times_in_town")
     */
    public function massTimesInTownAction(Request $request): JsonResponse
    {
        $townIds = explode(',', $request->query->get('townId', ''));
        $date = $request->query->get('date', date('Y-m-d'));

        $massesInTownData = $this->commonService->getMassesInTown($townIds, $date);
        $masses = array_merge(...array_values($massesInTownData['masses']));
        $times = array_map(function (Mass $mass) {
            return $mass->time;
        }, $masses);
        $uniqueTimes = array_unique($times);
        sort($uniqueTimes);

        return $this->json($uniqueTimes);
    }

    /**
     * @Route("/masses-in-church", name="app_masses_in_church")
     */
    public function massesInChurchAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');
        $regular = $this->massService->getChurchRegularMasses($id);
        $seasonal = $this->massService->getChurchSeasonalMasses($id);
        $extraordinary = $this->massService->getChurchExtraordinaryMasses($id);

        return $this->json([
            'regular' => $regular,
            'seasonal' => $seasonal,
            'extraordinary' => $extraordinary
        ]);
    }
}
