<?php

namespace App\Controller\Api;

use App\Service\Api\ArticleService;
use App\Service\Api\CalendarService;
use App\Service\Api\ImageService;
use App\Service\Api\TownService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class GeneralController extends AbstractController
{
    private $calendarService;
    private $imageService;
    private $articleService;
    private $townService;

    public function __construct(
        CalendarService $calendarService,
        ImageService $imageService,
        ArticleService $articleService,
        TownService $townService
    )
    {
        $this->calendarService = $calendarService;
        $this->imageService = $imageService;
        $this->articleService = $articleService;
        $this->townService = $townService;
    }

    /**
     * @Route("/special-dates", name="app_special_dates")
     */
    public function specialDatesAction(): JsonResponse
    {
        $specialDates = $this->calendarService->getSpecialDates();
        return $this->json($specialDates);
    }

    /**
     * @Route("/date-settings", name="app_date_settings")
     */
    public function dateSettingsAction(Request $request): JsonResponse
    {
        $date = $request->query->get('date', date('Y-m-d'));

        $dateSettings = $this->calendarService->getDateSettings($date);
        return $this->json($dateSettings);
    }

    /**
     * @Route("/holy-days", name="app_holy_days")
     */
    public function holyDaysAction(Request $request): JsonResponse
    {
        $date = $request->query->get('date', date('Y-m-d'));

        $holyDays = $this->calendarService->getHolyDays($date);
        return $this->json($holyDays);
    }

    /**
     * @Route("/images", name="app_images")
     */
    public function imagesAction(Request $request): JsonResponse
    {
        $ids = explode(',', $request->query->get('id', ''));

        $images = $this->imageService->getImages($ids);
        return $this->json($images);
    }

    /**
     * @Route("/article-by-alias", name="app_article_by_alias")
     */
    public function articleByAliasAction(Request $request): JsonResponse
    {
        $alias = $request->query->get('alias', '');

        $article = $this->articleService->getArticleByAlias($alias);
        return $this->json($article);
    }

    /**
     * @Route("/news-articles", name="app_news_articles")
     */
    public function newsArticlesAction(Request $request): JsonResponse
    {
        $news = $this->articleService->getNewsArticles();
        return $this->json($news);
    }

    /**
     * @Route("/all-towns", name="app_all_towns")
     */
    public function allTownsAction(Request $request): JsonResponse
    {
        $towns = $this->townService->getAllTowns();
        return $this->json($towns);
    }

    /**
     * @Route("/town-by-id", name="app_town_by_id")
     */
    public function townByIdAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');

        $town = $this->townService->getTownById($id);
        return $this->json($town);
    }

    /**
     * @Route("/towns-by-ids", name="app_towns_by_ids")
     */
    public function townsByIdsAction(Request $request): JsonResponse
    {
        $ids = explode(',', $request->query->get('id', ''));

        $towns = $this->townService->getTownsByIds($ids, false);
        return $this->json($towns);
    }

    /**
     * @Route("/towns-by-ids-expanded", name="app_towns_by_ids_expanded")
     */
    public function townsByIdsExpandedAction(Request $request): JsonResponse
    {
        $ids = explode(',', $request->query->get('id', ''));

        $towns = $this->townService->getTownsByIds($ids, true);
        return $this->json($towns);
    }

    /**
     * @Route("/town-id-by-alias", name="app_town_id_by_alias")
     */
    public function townIdByAliasAction(Request $request): JsonResponse
    {
        $aliases = explode(',', $request->query->get('alias', ''));

        $townIds = $this->townService->getTownIdsByAliases($aliases);
        return $this->json($townIds);
    }

    /**
     * @Route("/all-counties", name="app_all_counties")
     */
    public function allCountiesAction(Request $request): JsonResponse
    {
        $counties = $this->townService->getAllCounties();
        return $this->json($counties);
    }
}
