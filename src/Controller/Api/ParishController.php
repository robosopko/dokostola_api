<?php

namespace App\Controller\Api;

use App\Service\Api\ParishService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ParishController extends AbstractController
{
    private $parishService;

    public function __construct(ParishService $parishService)
    {
        $this->parishService = $parishService;
    }

    /**
     * @Route("/parish-by-alias", name="app_parish_by_alias")
     */
    public function parishByAliasAction(Request $request): JsonResponse
    {
        $alias = $request->query->get('alias', '');

        $parish = $this->parishService->getParishByAlias($alias);
        return $this->json($parish);
    }

    /**
     * @Route("/parish-by-id", name="app_parish_by_id")
     */
    public function parishByIdAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');

        $parish = $this->parishService->getParishById($id);
        return $this->json($parish);
    }

    /**
     * @Route("/all-parishes", name="app_all_parishes")
     */
    public function allParishesAction(Request $request): JsonResponse
    {
        $parishes = $this->parishService->getAllParishes();
        return $this->json($parishes);
    }

    /**
     * @Route("/parishes-in-deanery", name="app_parishes_in_deanery")
     */
    public function parishesInDeaneryAction(Request $request): JsonResponse
    {
        $deaneryIds = explode(',', $request->query->get('id', ''));

        $parishes = $this->parishService->getParishesInDeanery($deaneryIds);
        return $this->json($parishes);
    }

    /**
     * @Route("/parish-info", name="app_parish_info")
     */
    public function parishInfoAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');
        $alias = $request->query->get('alias');

        $parishDetails = $this->parishService->getParishInfo($id, $alias);
        return $this->json($parishDetails);
    }

    /**
     * @Route("/parish-announcements", name="app_parish_announcements")
     */
    public function parishAnnouncementsAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');

        $parishAnnouncements = $this->parishService->getParishAnnouncements($id);
        return $this->json($parishAnnouncements);
    }

}
