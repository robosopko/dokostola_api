<?php

namespace App\Controller\Api;

use App\Service\Api\ChurchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class ChurchController extends AbstractController
{
    private $churchService;

    public function __construct(ChurchService $churchService)
    {
        $this->churchService = $churchService;
    }

    /**
     * @Route("/churches", name="app_churches")
     */
    public function churchesAction(Request $request): JsonResponse
    {
        $churchIds = explode(',', $request->query->get('id', ''));
        
        $churches = $this->churchService->getChurches($churchIds);
        return $this->json($churches);
    }

    /**
     * @Route("/churches-in-town", name="app_churches_in_town")
     */
    public function churchesInTownAction(Request $request): JsonResponse
    {
        $bratislavaIds = ['30001','30002','30003','30004','30005','30006','30007','30008','30009','30010','30011','30012','30013','30014','30015','30016','30017'];
        $kosiceIds = ['32488','32489','32490','32491','32492','32493','32494','32495','32496','32497','32498','32499','32500','32501','32502','32503','32504','32505','32506','32507','32508','32509'];
        $townIds = explode(',', $request->query->get('id', ''));
        if (in_array('1', $townIds)) {
            $townIds = array_merge($townIds, $bratislavaIds);
        }
        if (in_array('2', $townIds)) {
            $townIds = array_merge($townIds, $kosiceIds);
        }

        $churchesInTown = $this->churchService->getChurchesInTown($townIds);
        return $this->json($churchesInTown);
    }

    /**
     * @Route("/churches-in-parish", name="app_churches_in_parish")
     */
    public function churchesInParishAction(Request $request): JsonResponse
    {
        $parishIds = explode(',', $request->query->get('id', ''));

        $churchesInParish = $this->churchService->getChurchesInParish($parishIds);
        return $this->json($churchesInParish);
    }

    /**
     * @Route("/all-churches", name="app_all_churches")
     */
    public function allChurchesAction(Request $request): JsonResponse
    {
        $churches = $this->churchService->getAllChurches();
        return $this->json($churches);
    }

    /**
     * @Route("/all-churches-gps", name="app_all_churches_gps")
     */
    public function allChurchesGpsAction(Request $request): JsonResponse
    {
        $churches = $this->churchService->getAllChurchesGps();
        return $this->json($churches);
    }

    /**
     * @Route("/all-churches-towns", name="app_all_churches_towns")
     */
    public function allChurchesTownsAction(Request $request): JsonResponse
    {
        $churches = $this->churchService->getAllChurchesTowns();
        return $this->json($churches);
    }

    /**
     * @Route("/church-info", name="app_church_info")
     */
    public function churchInfoAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');
        $alias = $request->query->get('alias');

        $churchDetails = $this->churchService->getChurchInfo($id, $alias);
        return $this->json($churchDetails);
    }

    /**
     * @Route("/church-announcements", name="app_church_announcements")
     */
    public function churchAnnouncementsAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');

        $churchAnnouncements = $this->churchService->getChurchAnnouncements($id);
        return $this->json($churchAnnouncements);
    }

}
