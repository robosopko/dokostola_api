<?php

namespace App\Controller\Api;

use App\Service\Api\DeaneryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class DeaneryController extends AbstractController
{
    private $deaneryService;

    public function __construct(DeaneryService $deaneryService)
    {
        $this->deaneryService = $deaneryService;
    }

    /**
     * @Route("/deanery-by-alias", name="app_deanery_by_alias")
     */
    public function deaneryByAliasAction(Request $request): JsonResponse
    {
        $alias = $request->query->get('alias', '');

        $deanery = $this->deaneryService->getDeaneryByAlias($alias);
        return $this->json($deanery);
    }

    /**
     * @Route("/deanery-by-id", name="app_deanery_by_id")
     */
    public function deaneryByIdAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');

        $deanery = $this->deaneryService->getDeaneryById($id);
        return $this->json($deanery);
    }

    /**
     * @Route("/deanery-info", name="app_deanery_info")
     */
    public function deaneryInfoAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');
        $alias = $request->query->get('alias');

        $deaneryInfo = $this->deaneryService->getDeaneryInfo($id, $alias);
        return $this->json($deaneryInfo);
    }

    /**
     * @Route("/deaneries-in-diocese", name="app_deaneries_in_diocese")
     */
    public function deaneriesInDioceseAction(Request $request): JsonResponse
    {
        $dioceseIds = explode(',', $request->query->get('id', ''));

        $deaneries = $this->deaneryService->getDeaneriesInDiocese($dioceseIds);
        return $this->json($deaneries);
    }

}
