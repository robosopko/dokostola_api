<?php

namespace App\Controller\Api;

use App\Service\Api\DioceseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class DioceseController extends AbstractController
{
    private $dioceseService;

    public function __construct(DioceseService $dioceseService)
    {
        $this->dioceseService = $dioceseService;
    }

    /**
     * @Route("/diocese-by-alias", name="app_diocese_by_alias")
     */
    public function dioceseByAliasAction(Request $request): JsonResponse
    {
        $alias = $request->query->get('alias', '');

        $diocese = $this->dioceseService->getDioceseByAlias($alias);
        return $this->json($diocese);
    }

    /**
     * @Route("/diocese-by-id", name="app_diocese_by_id")
     */
    public function dioceseByIdAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id', '');

        $diocese = $this->dioceseService->getDioceseById($id);
        return $this->json($diocese);
    }

    /**
     * @Route("/diocese-info", name="app_diocese_info")
     */
    public function dioceseInfoAction(Request $request): JsonResponse
    {
        $id = $request->query->get('id');
        $alias = $request->query->get('alias');

        $dioceseInfo = $this->dioceseService->getDioceseInfo($id, $alias);
        return $this->json($dioceseInfo);
    }

}
