<?php

namespace App\Controller\Web;

use App\Entity\Api\ChurchArrayItem;
use App\Entity\Api\Mass;
use App\Service\Api\MassService;
use App\Service\CommonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private $commonService;

    public function __construct(
        CommonService $commonService
    )
    {
        $this->commonService = $commonService;
    }

    /**
     * @Route("/bohosluzby", name="app_web_bohosluzby")
     */
    public function searchMassesAction(Request $request): Response
    {
        $townIds = explode(',', $request->query->get('obec', ''));
        $date = $request->query->get('datum', date('Y-m-d'));

        $massesInTownData = $this->commonService->getMassesInTown($townIds, $date);
        /**
         * @var ChurchArrayItem[] $churches
         */
        $churches = $massesInTownData['churches'];
        $massesByChurchId = $massesInTownData['masses'];

        $massesByTime = [];
        foreach ($massesByChurchId as $churchId => $churchMasses) {
            if(empty($churchMasses))
                continue;

            /**
             * @var Mass $mass
             */
            foreach ($churchMasses as $mass) {
                $massesByTime[$mass->time][] = [
                    'churchName' => $churches[$churchId]->title,
                    'note' => $mass->note
                ];
            }

        }

        ksort($massesByTime);

        return $this->render('search/masses_by_time.html.twig', [
            'massesByTime' => $massesByTime
        ]);
    }
}
