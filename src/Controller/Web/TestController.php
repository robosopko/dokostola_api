<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("test")
 */
class TestController extends AbstractController
{
    /**
     * @Route("/tailwind", name="app_test_tailwind")
     */
    public function tailwindAction(): Response
    {
        return $this->render('test/tailwind_example.html.twig');
    }
}
