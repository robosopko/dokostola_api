<?php


namespace App;


class Utils
{
    public static function objectKeys($obj)
    {
        return array_keys(get_object_vars($obj));
    }

    public static function mergeObjects($obj1, $obj2)
    {
        return (object) array_merge((array) $obj1, (array) $obj2);
    }

    public static function skToIsoDateString($skDate)
    {
        $dateStringArray = explode('.', $skDate);
        return "{$dateStringArray[2]}-{$dateStringArray[1]}-{$dateStringArray[0]}";
    }
}
