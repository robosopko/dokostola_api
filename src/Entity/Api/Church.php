<?php

namespace App\Entity\Api;

use App\Utils;

class Church
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $typeId;
    /**
     * @var int
     */
    public $parishId;
    /**
     * @var int
     */
    public $townId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var Image[]
     */
    public $images;
    /**
     * @var string
     */
    public $seasonMassesFrom;
    /**
     * @var string
     */
    public $seasonMassesTo;
    /**
     * @var string
     */
    public $seasonConfessionsFrom;
    /**
     * @var string
     */
    public $seasonConfessionsTo;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $gpsCoordinates;
    /**
     * @var string
     */
    public $aboutChurch;
    /**
     * @var string
     */
    public $history;
    /**
     * @var string
     */
    public $notes;

    public static function getChurchDetailsSql(bool $useAlias): string
    {
        $column = $useAlias ? 'kostol.t_alias' : 'kostol.i_id_item';

        return "select 
            kostol.i_id_item, 
            kostol.t_title, 
            kostol.t_alias, 
            kostol_info.i_id_param, 
            kostol_info.t_value
        from catalog_item as kostol
        join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where $column = ? 
            and kostol.i_id_catalog = 4 
            and kostol.s_pub = '1'
        ";
    }

    public static function buildFromSqlResult($resultArray): Church
    {
        $church = new Church();
        foreach ($resultArray as $result) {
            $church->id = intval($result['i_id_item']);
            $church->title = $result['t_title'];
            $church->alias = $result['t_alias'];

            switch ($result['i_id_param']) {
                case ChurchParams::Type:
                    $church->typeId = intval($result['t_value']);
                    break;
                case ChurchParams::Parish:
                    $church->parishId = intval($result['t_value']);
                    break;
                case ChurchParams::Town:
                    $church->townId = intval($result['t_value']);
                    break;
                case ChurchParams::SeasonMassesFrom:
                    $church->seasonMassesFrom = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonMassesTo:
                    $church->seasonMassesTo = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonConfessionsFrom:
                    $church->seasonConfessionsFrom = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonConfessionsTo:
                    $church->seasonConfessionsTo = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::Address:
                    $church->address = $result['t_value'];
                    break;
                case ChurchParams::GpsCoordinates:
                    $church->gpsCoordinates = $result['t_value'];
                    break;
                case ChurchParams::AboutChurch:
                    $church->aboutChurch = $result['t_value'];
                    break;
                case ChurchParams::History:
                    $church->history = $result['t_value'];
                    break;
                case ChurchParams::Notes:
                    $church->notes = $result['t_value'];
                    break;
            }
        }

        return $church;
    }
}