<?php


namespace App\Entity\Api;


class Image
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $parentId;
    /**
     * @var string
     */
    public $fullImage;
    /**
     * @var string
     */
    public $thumbImage;
    /**
     * @var string
     */
    public $title;
    /**
     * @var int
     */
    public $fullWidth;
    /**
     * @var int
     */
    public $fullHeight;
    /**
     * @var int
     */
    public $thumbWidth;
    /**
     * @var int
     */
    public $thumbHeight;

    public static function buildFromSqlResult($result): Image
    {
        $image = new Image();
        $image->id = intval($result['id']);
        $image->parentId = intval($result['parentId']);
        $image->title = $result['title'];
        $image->fullWidth = intval($result['fullWidth']);
        $image->fullHeight = intval($result['fullHeight']);
        $image->thumbWidth = intval($result['thumbWidth']);
        $image->thumbHeight = intval($result['thumbHeight']);
        if (!empty($result['fullImage'])) {
            $basePath = 'https://portal.dokostola.dev/content/images/' . substr($result['fullImage'], 0, 1) . '/';
            $image->fullImage = $basePath . $result['fullImage'];
            $image->thumbImage = $basePath . $result['thumbImage'];
        }
        return $image;
    }
}