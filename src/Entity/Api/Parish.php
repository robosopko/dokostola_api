<?php


namespace App\Entity\Api;


class Parish
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var int
     */
    public $deaneryId;
    /**
     * @var int
     */
    public $townId;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $gpsCoordinates;
    /**
     * @var string
     */
    public $administrator;
    /**
     * @var string
     */
    public $feastDay;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $cellular;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $web;
    /**
     * @var string
     */
    public $about;
    /**
     * @var string
     */
    public $history;
    /**
     * @var string
     */
    public $hours;
    /**
     * @var string
     */
    public $notes;
    /**
     * @var string
     */
    public $announcements;

    public static function getParishDetailsSql(bool $useAlias): string
    {
        $column = $useAlias ? 'farnost.t_alias' : 'farnost.i_id_item';

        return "select 
            farnost_info.i_id_item, 
            farnost.t_alias, 
            farnost.t_title, 
            farnost_info.i_id_param, 
            farnost_info.t_value
        from catalog_item_param_value as farnost_info
        join catalog_item as farnost on farnost.i_id_item = farnost_info.i_id_item
        where $column = ? 
            and farnost_info.i_id_param in (3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3019)
            and farnost.i_id_catalog = 3 
            and farnost.s_pub = '1'
        ";
    }

    public static function buildFromSqlResult($resultArray): Parish
    {
        $parish = new Parish();
        foreach ($resultArray as $result) {
            $parish->id = intval($result['i_id_item']);
            $parish->title = $result['t_title'];
            $parish->alias = $result['t_alias'];

            switch ($result['i_id_param']) {
                case ChurchParams::Deanery:
                    $parish->deaneryId = intval($result['t_value']);
                    break;
                case ChurchParams::ParishTown:
                    $parish->townId = intval($result['t_value']);
                    break;
                case ChurchParams::ParishAddress:
                    $parish->address = $result['t_value'];
                    break;
                case ChurchParams::ParishGpsCoordinates:
                    $parish->gpsCoordinates = $result['t_value'];
                    break;
                case ChurchParams::ParishAdministrator:
                    $parish->administrator = $result['t_value'];
                    break;
                case ChurchParams::ParishFeastDay:
                    $parish->feastDay = $result['t_value'];
                    break;
                case ChurchParams::ParishPhone:
                    $parish->phone = $result['t_value'];
                    break;
                case ChurchParams::ParishCellular:
                    $parish->cellular = $result['t_value'];
                    break;
                case ChurchParams::ParishEmail:
                    $parish->email = $result['t_value'];
                    break;
                case ChurchParams::ParishWeb:
                    $parish->web = $result['t_value'];
                    break;
                case ChurchParams::ParishAbout:
                    $parish->about = $result['t_value'];
                    break;
                case ChurchParams::ParishHistory:
                    $parish->history = $result['t_value'];
                    break;
                case ChurchParams::ParishHours:
                    $parish->hours = $result['t_value'];
                    break;
                case ChurchParams::ParishNotes:
                    $parish->notes = $result['t_value'];
                    break;
                case ChurchParams::ParishAnnouncements:
                    $parish->announcements = $result['t_value'];
                    break;
            }
        }

        return $parish;
    }
}
