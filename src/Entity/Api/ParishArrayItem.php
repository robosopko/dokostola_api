<?php


namespace App\Entity\Api;


class ParishArrayItem
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $deaneryId;

    public static function buildFromSqlResult($result): ParishArrayItem
    {
        $parishArrayItem = new ParishArrayItem();
        $parishArrayItem->id = intval($result['id']);
        $parishArrayItem->title = $result['title'];
        $parishArrayItem->alias = $result['alias'];
        $parishArrayItem->deaneryId = isset($result['deaneryId']) ? intval($result['deaneryId']) : null;
        return $parishArrayItem;
    }
}