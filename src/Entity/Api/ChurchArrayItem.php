<?php


namespace App\Entity\Api;


class ChurchArrayItem
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $townId;
    /**
     * @var int
     */
    public $parishId;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var Image[]
     */
    public $images;

    public static function buildFromSqlResult($result): ChurchArrayItem
    {
        $churchArrayItem = new ChurchArrayItem();
        $churchArrayItem->id = intval($result['id']);
        $churchArrayItem->townId = isset($result['townId']) ? intval($result['townId']) : null;
        $churchArrayItem->parishId = isset($result['parishId']) ? intval($result['parishId']) : null;
        $churchArrayItem->title = $result['title'];
        $churchArrayItem->alias = $result['alias'];
        $churchArrayItem->images = [];
        return $churchArrayItem;
    }
}
