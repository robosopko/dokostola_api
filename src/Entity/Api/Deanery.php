<?php


namespace App\Entity\Api;


class Deanery
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $dioceseId;
    /**
     * @var string
     */
    public $townId;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $web;

    public static function buildBasicFromSqlResult($result): Deanery
    {
        $deanery = new Deanery();
        $deanery->id = intval($result['id']);
        $deanery->title = $result['title'];
        $deanery->alias = $result['alias'];
        $deanery->dioceseId = isset($result['dioceseId']) ? intval($result['dioceseId']) : null;
        $deanery->townId = isset($result['$townId']) ? intval($result['$townId']) : null;
        return $deanery;
    }

    public static function getDeaneryDetailsSql(bool $useAlias): string
    {
        $column = $useAlias ? 'dekanat.t_alias' : 'dekanat.i_id_item';

        return "select 
            dekanat_info.i_id_item, 
            dekanat.t_alias, 
            dekanat.t_title, 
            dekanat_info.i_id_param, 
            dekanat_info.t_value
        from catalog_item_param_value as dekanat_info
        join catalog_item as dekanat on dekanat.i_id_item = dekanat_info.i_id_item
        where $column = ? 
            and dekanat_info.i_id_param in (2201, 2202, 2203, 2204, 2205, 2206)
            and dekanat.i_id_catalog = 2 
            and dekanat.i_id_category = 2 
            and dekanat.s_pub = '1'
        ";
    }

    public static function buildExtendedFromSqlResult($resultArray): Deanery
    {
        $deanery = new Deanery();
        foreach ($resultArray as $result) {
            $deanery->id = intval($result['i_id_item']);
            $deanery->title = $result['t_title'];
            $deanery->alias = $result['t_alias'];

            switch (intval($result['i_id_param'])) {
                case ChurchParams::Diocese:
                    $deanery->dioceseId = intval($result['t_value']);
                    break;
                case ChurchParams::DeaneryTown:
                    $deanery->townId = intval($result['t_value']);
                    break;
                case ChurchParams::DeaneryAddress:
                    $deanery->address = $result['t_value'];
                    break;
                case ChurchParams::DeaneryPhone:
                    $deanery->phone = $result['t_value'];
                    break;
                case ChurchParams::DeaneryEmail:
                    $deanery->email = $result['t_value'];
                    break;
                case ChurchParams::DeaneryWeb:
                    $deanery->web = $result['t_value'];
                    break;
            }
        }
        return $deanery;
    }
}
