<?php


namespace App\Entity\Api;


class DateSettings
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $celebration;
    /**
     * @var string
     */
    public $celebrationRank;
    /**
     * @var bool
     */
    public $isStateHoliday;
    /**
     * @var bool
     */
    public $isObligatoryFeast;
    /**
     * @var bool
     */
    public $isHabitualFeast;

    public static function buildFromSqlResult($result): DateSettings
    {
        $dateSettings = new DateSettings();
        $dateSettings->date = $result['date'];
        $dateSettings->celebration = $result['celebration'];
        $dateSettings->celebrationRank = $result['celebrationRank'];
        $dateSettings->isStateHoliday = boolval($result['isStateHoliday']);
        $dateSettings->isObligatoryFeast = boolval($result['isObligatoryFeast']);
        $dateSettings->isHabitualFeast = boolval($result['isHabitualFeast']);
        return $dateSettings;
    }

    public static function dateSettingsToDayOfWeek(DateSettings $dateSettings): int
    {
        $dayOfWeek = intval(date('N', strtotime($dateSettings->date)));

        if ($dateSettings->isObligatoryFeast || $dateSettings->isHabitualFeast) {
            if ($dateSettings->isStateHoliday || $dayOfWeek == Day::Saturday || $dayOfWeek == Day::Sunday) {
                return Day::RestdayFeast;
            } else {
                return Day::WorkdayFeast;
            }
        } else {
            return $dayOfWeek;
        }
    }
}
