<?php


namespace App\Entity\Api;


class Diocese
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $web;

    public static function buildBasicFromSqlResult($result): Diocese
    {
        $diocese = new Diocese();
        $diocese->id = intval($result['id']);
        $diocese->title = $result['title'];
        $diocese->alias = $result['alias'];
        return $diocese;
    }

    public static function getDioceseDetailsSql(bool $useAlias): string
    {
        $column = $useAlias ? 'dieceza.t_alias' : 'dieceza.i_id_item';

        return "select 
            dieceza_info.i_id_item, 
            dieceza.t_alias, 
            dieceza.t_title, 
            dieceza_info.i_id_param, 
            dieceza_info.t_value
        from catalog_item_param_value as dieceza_info
        join catalog_item as dieceza on dieceza.i_id_item = dieceza_info.i_id_item
        where $column = ? 
            and dieceza_info.i_id_param in (2101, 2102, 2103, 2104)
            and dieceza.i_id_catalog = 2 
            and dieceza.i_id_category = 1 
            and dieceza.s_pub = '1'
        ";
    }

    public static function buildExtendedFromSqlResult($resultArray): Diocese
    {
        $diocese = new Diocese();
        foreach ($resultArray as $result) {
            $diocese->id = intval($result['i_id_item']);
            $diocese->title = $result['t_title'];
            $diocese->alias = $result['t_alias'];

            switch ($result['i_id_param']) {
                case ChurchParams::DioceseAddress:
                    $diocese->address = $result['t_value'];
                    break;
                case ChurchParams::DiocesePhone:
                    $diocese->phone = $result['t_value'];
                    break;
                case ChurchParams::DioceseEmail:
                    $diocese->email = $result['t_value'];
                    break;
                case ChurchParams::DioceseWeb:
                    $diocese->web = $result['t_value'];
                    break;
            }
        }
        return $diocese;
    }
}
