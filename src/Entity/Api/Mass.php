<?php


namespace App\Entity\Api;


class Mass
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $churchId;
    /**
     * @var Day
     */
    public $day;
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $time;
    /**
     * @var string
     */
    public $languageId;
    /**
     * @var string
     */
    public $focusId;
    /**
     * @var string
     */
    public $note;
    /**
     * @var bool
     */
    public $isExtraordinary;
    /**
     * @var bool
     */
    public $overrideRegular;
    /**
     * @var bool
     */
    public $noMassToday;
    /**
     * @var bool
     */
    public $isSeasonal;

    public static function buildFromSqlResult($result): Mass
    {
        $mass = new Mass();
        $mass->id = intval($result['id']);
        $mass->churchId = intval($result['churchId']);
        $mass->day = isset($result['day']) ? intval($result['day']) : 0;
        $mass->date = isset($result['date']) ? $result['date'] : null;
        $mass->time = $result['time'];
        $mass->languageId = intval($result['languageId']);
        $mass->focusId = intval($result['focusId']);
        $mass->note = $result['note'];
        $mass->isExtraordinary = isset($result['date']) ? boolval($result['date']) : false;
        $mass->overrideRegular = isset($result['overrideRegular']) ? boolval($result['overrideRegular']) : false;
        $mass->noMassToday = isset($result['noMassToday']) ? boolval($result['noMassToday']) : false;
        $mass->isSeasonal = isset($result['isSeasonal']) ? boolval($result['isSeasonal']) : false;
        return $mass;
    }
}
