<?php


namespace App\Entity\Api;


class Article
{
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $author;
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $perex;
    /**
     * @var string
     */
    public $text;

    public static function buildFromSqlResult($result): Article
    {
        $article = new Article();
        $article->alias = $result['alias'];
        $article->title = $result['title'];
        $article->author = $result['author'];
        $article->date = $result['date'];
        $article->perex = $result['perex'];
        $article->text = $result['text'];
        return $article;
    }
}