<?php


namespace App\Entity\Api;


class Confession
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $churchId;
    /**
     * @var Day
     */
    public $day;
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $time;
    /**
     * @var string
     */
    public $endTime;
    /**
     * @var string
     */
    public $languageId;
    /**
     * @var string
     */
    public $note;
    /**
     * @var bool
     */
    public $isExtraordinary;
    /**
     * @var bool
     */
    public $overrideRegular;
    /**
     * @var bool
     */
    public $noConfessionToday;
    /**
     * @var bool
     */
    public $isSeasonal;

    public static function buildFromSqlResult($result): Confession
    {
        $confession = new Confession();
        $confession->id = intval($result['id']);
        $confession->churchId = intval($result['churchId']);
        $confession->day = isset($result['day']) ? intval($result['day']) : 0;
        $confession->date = isset($result['date']) ? $result['date'] : null;
        $confession->time = $result['time'];
        $confession->endTime = $result['endTime'];
        $confession->languageId = intval($result['languageId']);
        $confession->note = $result['note'];
        $confession->isExtraordinary = isset($result['date']) ? boolval($result['date']) : false;
        $confession->overrideRegular = isset($result['overrideRegular']) ? boolval($result['overrideRegular']) : false;
        $confession->noConfessionToday = isset($result['noConfessionToday']) ? boolval($result['noConfessionToday']) : false;
        $confession->isSeasonal = isset($result['isSeasonal']) ? boolval($result['isSeasonal']) : false;
        return $confession;
    }
}