<?php


namespace App\Entity\Api;


class Town
{
    /**
     * @var string
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $countyId;

    public static function buildFromSqlResult($result): Town
    {
        $town = new Town();
        $town->id = intval($result['id']);
        $town->title = $result['title'];
        $town->alias = $result['alias'];
        $town->countyId = intval($result['countyId']);
        return $town;
    }
}