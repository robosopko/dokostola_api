<?php


namespace App\Entity\Api;


use App\Utils;

class ChurchBasicInfo
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $typeId;
    /**
     * @var int
     */
    public $parishId;
    /**
     * @var int
     */
    public $townId;
    /**
     * @var string
     */
    public $seasonMassesFrom;
    /**
     * @var string
     */
    public $seasonMassesTo;
    /**
     * @var string
     */
    public $seasonConfessionsFrom;
    /**
     * @var string
     */
    public $seasonConfessionsTo;


    /**
     * @param array $resultArray
     * @return ChurchBasicInfo[]
     */
    public static function buildFromSqlResult(array $resultArray): array
    {
        $churches = [];
        foreach ($resultArray as $result) {
            $id = intval($result['i_id_item']);
            $church = isset($churches[$id]) ? $churches[$id] : new ChurchBasicInfo();
            $church->id = $id;

            switch ($result['i_id_param']) {
                case ChurchParams::Type:
                    $church->typeId = intval($result['t_value']);
                    break;
                case ChurchParams::Parish:
                    $church->parishId = intval($result['t_value']);
                    break;
                case ChurchParams::Town:
                    $church->townId = intval($result['t_value']);
                    break;
                case ChurchParams::SeasonMassesFrom:
                    $church->seasonMassesFrom = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonMassesTo:
                    $church->seasonMassesTo = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonConfessionsFrom:
                    $church->seasonConfessionsFrom = Utils::skToIsoDateString($result['t_value']);
                    break;
                case ChurchParams::SeasonConfessionsTo:
                    $church->seasonConfessionsTo = Utils::skToIsoDateString($result['t_value']);
                    break;
            }
            $churches[$id] = $church;
        }

        return $churches;
    }
}
