<?php


namespace App\Entity\Api;


class ChurchParams
{
    const Type = 4002;
    const Parish = 4003;
    const Town = 4004;
    const SeasonMassesFrom = 4101;
    const SeasonMassesTo = 4102;
    const SeasonConfessionsFrom = 4201;
    const SeasonConfessionsTo = 4202;

    const Address = 4007;
    const GpsCoordinates = 4008;
    const AboutChurch = 4011;
    const History = 4012;
    const Notes = 4013;
    const Announcements = 4015;

    const Deanery = 3002;
    const ParishTown = 3003;
    const ParishAddress = 3005;
    const ParishGpsCoordinates = 3006;
    const ParishAdministrator = 3007;
    const ParishFeastDay = 3008;
    const ParishPhone = 3009;
    const ParishCellular = 3011;
    const ParishEmail = 3012;
    const ParishWeb = 3013;
    const ParishAbout = 3014;
    const ParishHistory = 3015;
    const ParishHours = 3016;
    const ParishNotes = 3017;
    const ParishAnnouncements = 3019;

    const DioceseAddress = 2101;
    const DiocesePhone = 2102;
    const DioceseEmail = 2103;
    const DioceseWeb = 2104;

    const Diocese = 2201;
    const DeaneryTown = 2202;
    const DeaneryAddress = 2203;
    const DeaneryPhone = 2204;
    const DeaneryEmail = 2205;
    const DeaneryWeb = 2206;
}