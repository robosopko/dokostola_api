<?php


namespace App\Service;


use App\Entity\Api\ChurchBasicInfo;
use App\Entity\Api\DateSettings;
use App\Entity\Api\Mass;
use App\Service\Api\CalendarService;
use App\Service\Api\ChurchService;
use App\Service\Api\ImageService;
use App\Service\Api\MassService;
use App\Service\Api\TownService;
use App\Utils;

class CommonService
{
    private $churchService;
    private $massService;
    private $imageService;
    private $calendarService;
    private $townService;

    public function __construct(
        ChurchService $churchService,
        MassService $massService,
        ImageService $imageService,
        CalendarService $calendarService,
        TownService $townService
    )
    {
        $this->churchService = $churchService;
        $this->massService = $massService;
        $this->imageService = $imageService;
        $this->calendarService = $calendarService;
        $this->townService = $townService;
    }

    public function getMassesInChurch(array $churchIds, array $churches, string $date): array
    {
        if (count($churches) == 0) {
            $churches = $this->churchService->getChurches($churchIds);
        }
        
        $churchBasicInfoArray = $this->churchService->getChurchBasicInfo($churchIds);

        foreach ($churchBasicInfoArray as $churchId => $churchBasicInfo) {
            $church = $churches[$churchId];
            $churches[$churchId] = Utils::mergeObjects($church, $churchBasicInfo);
        }

        $churchImagesArray = $this->imageService->getImages($churchIds);

        foreach ($churchImagesArray as $image) {
            array_push($churches[$image->parentId]->images, $image);
        }

        $dateSettings = $this->calendarService->getDateSettings($date);

        $regularMasses = $this->massService->getRegularMasses($churchIds, DateSettings::dateSettingsToDayOfWeek($dateSettings), false);
        $regularMassesInSeason = $this->massService->getRegularMasses($churchIds, DateSettings::dateSettingsToDayOfWeek($dateSettings), true);
        $extraordinaryMasses = $this->massService->getExtraordinaryMasses($churchIds, $date);

        $masses = [];
        /**
         * @var ChurchBasicInfo $church
         */
        foreach ($churches as $churchId => $church) {
            /**
             * @var Mass[] $churchMasses
             */
            $churchMasses = [];
            /**
             * @var Mass[] $churchExtraordinaryMasses
             */
            $churchExtraordinaryMasses = isset($extraordinaryMasses[$churchId]) ? $extraordinaryMasses[$churchId] : [];

            $noMassToday = count(
                    array_filter($churchExtraordinaryMasses, function($mass) {
                        return $mass->noMassToday == true;
                    })
                ) > 0;

            if ($noMassToday) {
                $masses[$churchId] = [];
                continue;
            }

            $churchMasses = array_merge($churchMasses, $churchExtraordinaryMasses);

            $overrideRegular = count(
                    array_filter($churchExtraordinaryMasses, function($mass) {
                        return $mass->overrideRegular == true;
                    })
                ) > 0;

            if ($overrideRegular || CalendarService::isMassSpecialDate($dateSettings->date)) {
                $masses[$churchId] = $churchMasses;
                continue;
            }

            if (CalendarService::isInSeason($dateSettings->date, $church->seasonMassesFrom, $church->seasonMassesTo)) {
                $churchMasses = isset($regularMassesInSeason[$churchId]) ? array_merge($churchMasses, $regularMassesInSeason[$churchId]) : $churchMasses;
            } else {
                $churchMasses = isset($regularMasses[$churchId]) ? array_merge($churchMasses, $regularMasses[$churchId]) : $churchMasses;
            };

            $masses[$churchId] = $churchMasses;
        }

        return [
            'churches' => $churches,
            'masses' => $masses
        ];
    }

    public function getMassesInTown(array $townIds, string $date): array
    {
        if (count($townIds) > 0 && !is_numeric($townIds[0])) {
            $townIds = $this->townService->getTownIdsByAliases($townIds);
        }

        $townIds = $this->townService->expandTownIds($townIds);
        $churches = $this->churchService->getChurchesInTown($townIds);
        $churchIds = array_keys($churches);

        return $this->getMassesInChurch($churchIds, $churches, $date);
    }

}
