<?php

namespace App\Service\Api;

use App\Entity\Api\Image;
use Doctrine\DBAL\Driver\Connection;

class ImageService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $ids
     * @return Image[]
     */
    function getImages(array $ids): array
    {
        $sql = "
        select 
            image.i_id_image as id, 
            binder.i_id_a as parentId, 
            image.t_title as title, 
            image.t_full_image as fullImage, 
            image.t_thumb_image as thumbImage,
            image.s_full_width as fullWidth, 
            image.s_full_height as fullHeight,
            image.s_thumb_width as thumbWidth, 
            image.s_thumb_height as thumbHeight
        from com_binder as binder 
            join com_image as image on binder.i_id_b = image.i_id_image
        where binder.i_id_a in (?) 
            and binder.s_pub = '1' 
            and binder.t_name_b = 'image'
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$ids], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $images = [];
        foreach ($resultArray as $result) {
            $images[] = Image::buildFromSqlResult($result);
        }
        return $images;
    }
}
