<?php

namespace App\Service\Api;

use App\Entity\Api\Article;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;

class ArticleService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $alias Article alias
     * @return Article
     */
    function getArticleByAlias(string $alias): Article
    {
        $sql = "
        select 
            t_alias as alias, 
            t_title as title, 
            t_perex as perex, 
            t_text as text, 
            t_author as author,
            d_lastupdate as date
        from com_article
        where t_alias = ? 
        ";
        $result = $this->connection->fetchAssociative($sql, [$alias], [Types::ASCII_STRING]);

        return Article::buildFromSqlResult($result);
    }

    /**
     * @return Article[] array of news articles
     */
    function getNewsArticles(): array
    {
        $sql = "
        select 
            t_alias as alias, 
            t_title as title, 
            t_perex as perex, 
            t_text as text, 
            t_author as author,
            d_lastupdate as date
        from com_article
        where i_id_category = 1 and s_pub = '1'
        order by n_order
        ";
        $result = $this->connection->fetchAllAssociative($sql);

        $news = [];
        foreach ($result as $item) {
            $news[] = Article::buildFromSqlResult($item);
        }

        return $news;
    }
}
