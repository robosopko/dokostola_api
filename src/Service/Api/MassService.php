<?php

namespace App\Service\Api;

use App\Entity\Api\Mass;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;

class MassService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $churchIds
     * @param string $date
     * @return array associative array of Mass arrays by churchId
     */
    function getExtraordinaryMasses(array $churchIds, string $date): array
    {
        $sql = "
        select 
            i_id_bs as id, 
            i_id_kostol as churchId, 
            d_date as date, 
            d_time as time, 
            i_id_jazyk as languageId, 
            i_id_zameranie as focusId, 
            t_note as note, 
            s_sezonna as isSeasonal, 
            s_iba as overrideRegular, 
            s_bez as noMassToday
        from bohosluzby 
        where i_id_kostol in (?) 
            and d_date = ?
        order by d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchIds, $date], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY, Types::ASCII_STRING]);

        $masses = [];
        foreach ($resultArray as $result) {
            $masses[$result['churchId']][] = Mass::buildFromSqlResult($result);
        }
        return $masses;
    }

    /**
     * @param string $churchId
     * @return Mass[]
     */
    function getChurchExtraordinaryMasses(string $churchId): array
    {
        $sql = "
        select 
            i_id_bs as id, 
            i_id_kostol as churchId, 
            d_date as date, 
            d_time as time, 
            i_id_jazyk as languageId, 
            i_id_zameranie as focusId, 
            t_note as note, 
            s_sezonna as isSeasonal, 
            s_iba as overrideRegular, 
            s_bez as noMassToday
        from bohosluzby 
        where i_id_kostol = ? 
            and d_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) 
        order by d_date, d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchId], [Types::INTEGER]);

        $masses = [];
        foreach ($resultArray as $result) {
            $masses[] = Mass::buildFromSqlResult($result);
        }
        return $masses;
    }

    /**
     * @param array $churchIds
     * @param int $day
     * @param bool $isSeason
     * @return array associative array of Mass arrays by church id
     */
    function getRegularMasses(array $churchIds, int $day, bool $isSeason): array
    {
        $sql = "
        select 
            i_id_bs as id, 
            i_id_kostol as churchId, 
            i_id_den as day, 
            d_time as time, 
            i_id_jazyk as languageId, 
            i_id_zameranie as focusId, 
            t_note as note, 
            s_sezonna as isSeasonal
        from bohosluzby 
        where i_id_kostol in (?) 
            and i_id_den = ? 
            and s_sezonna = ? 
        order by d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchIds, $day, $isSeason ? '1' : '0'], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY, Types::ASCII_STRING, Types::ASCII_STRING]);

        $masses = [];
        foreach ($resultArray as $result) {
            $masses[$result['churchId']][] = Mass::buildFromSqlResult($result);
        }
        return $masses;
    }

    /**
     * @param string $churchId
     * @param bool $isSeason
     * @return Mass[]
     */
    function getChurchRegularMassesByIdAndSeason(string $churchId, bool $isSeason): array
    {
        $sql = "
        select 
            i_id_bs as id, 
            i_id_kostol as churchId, 
            i_id_den as day, 
            d_time as time, 
            i_id_jazyk as languageId, 
            i_id_zameranie as focusId, 
            t_note as note, 
            s_sezonna as isSeasonal
        from bohosluzby where i_id_kostol = ? 
            and i_id_den > 0 
            and s_sezonna = ?
        order by i_id_den, d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchId, $isSeason ? '1' : '0'], [Types::ASCII_STRING, Types::ASCII_STRING]);

        $masses = [];
        foreach ($resultArray as $result) {
            $masses[] = Mass::buildFromSqlResult($result);
        }
        return $masses;
    }

    /**
     * @param string $churchId
     * @return Mass[]
     */
    function getChurchRegularMasses(string $churchId): array
    {
        return $this->getChurchRegularMassesByIdAndSeason($churchId, false);
    }

    /**
     * @param string $churchId
     * @return Mass[]
     */
    function getChurchSeasonalMasses(string $churchId): array
    {
        return $this->getChurchRegularMassesByIdAndSeason($churchId, true);
    }

}
