<?php

namespace App\Service\Api;

use App\Entity\Api\ChurchParams;
use App\Entity\Api\Deanery;
use App\Entity\Api\Diocese;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;
use stdClass;

class DioceseService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    function getDioceseByAlias(string $alias): Diocese
    {
        $sql = "
        select 
            dieceza.i_id_item as id, 
            dieceza.t_title as title, 
            dieceza.t_alias as alias 
        from catalog_item as dieceza
        where dieceza.t_alias = ? 
            and dieceza.i_id_catalog = 2 
            and dieceza.i_id_category = 1 
            and dieceza.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$alias], [Types::ASCII_STRING]);

        return Diocese::buildBasicFromSqlResult($result);
    }

    function getDioceseById(string $id): Diocese
    {
        $sql = "
        select 
            dieceza.i_id_item as id, 
            dieceza.t_title as title, 
            dieceza.t_alias as alias 
        from catalog_item as dieceza
        where dieceza.i_id_item = ? 
            and dieceza.i_id_catalog = 2 
            and dieceza.i_id_category = 1 
            and dieceza.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        return Diocese::buildBasicFromSqlResult($result);
    }

    function getDioceseInfo(?string $id, ?string $alias): ?Diocese
    {
        $useAlias = !empty($alias);
        $sql = Diocese::getDioceseDetailsSql($useAlias);
        $param = $useAlias ? $alias : $id;
        $resultArray = $this->connection->fetchAllAssociative($sql, [$param], [Types::ASCII_STRING]);
        if (empty($resultArray)) {
            return null;
        }

        return Diocese::buildExtendedFromSqlResult($resultArray);
    }

}
