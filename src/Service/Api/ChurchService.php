<?php

namespace App\Service\Api;

use App\Entity\Api\Church;
use App\Entity\Api\ChurchArrayItem;
use App\Entity\Api\ChurchBasicInfo;
use App\Entity\Api\ChurchParams;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;
use stdClass;

class ChurchService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $churchIds
     * @return array associative array containing ChurchArrayItem objects by id
     */
    public function getChurches(array $churchIds): array
    {
        $sql = "
        select
            kostol.i_id_item as id, 
            kostol.t_title as title, 
            kostol.t_alias as alias, 
            kostol_info.t_value as townId
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol_info.i_id_param = 4004 
            and kostol.i_id_item in (?) 
            and kostol.s_pub = '1'
        ";
        $result = $this->connection->fetchAllAssociative($sql, [$churchIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $churches = [];
        foreach ($result as $item) {
            $churches[$item['id']] = ChurchArrayItem::buildFromSqlResult($item);
        }

        return $churches;
    }

    /**
     * @param array $townIds
     * @return array associative array containing ChurchArrayItem objects by id
     */
    public function getChurchesInTown(array $townIds): array
    {
        $sql = "
        select
            kostol.i_id_item as id, 
            kostol.t_title as title, 
            kostol.t_alias as alias, 
            kostol_info.t_value as townId
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol_info.i_id_param = 4004 
            and kostol_info.t_value in (?) 
            and kostol.s_pub = '1'
        ";
        $result = $this->connection->fetchAllAssociative($sql, [$townIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $churches = [];
        foreach ($result as $item) {
            $churches[$item['id']] = ChurchArrayItem::buildFromSqlResult($item);
        }

        return $churches;
    }

    /**
     * @param array $parishIds
     * @return stdClass object containing ChurchArrayItem objects
     */
    function getChurchesInParish(array $parishIds): stdClass
    {
        $sql = "
        select 
               kostol.i_id_item as id, 
               kostol.t_title as title, 
               kostol.t_alias as alias, 
               kostol_info.t_value as parishId
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol_info.i_id_param = 4003 
            and kostol_info.t_value in (?) 
            and kostol.s_pub = '1'
        ";
        $result = $this->connection->fetchAllAssociative($sql, [$parishIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $churches = new stdClass();
        foreach ($result as $item) {
            $church = ChurchArrayItem::buildFromSqlResult($item);
            $churches->{$church->id} = $church;
        }

        return $churches;
    }

    function getChurchInfo(?string $id, ?string $alias): ?Church
    {
        $useAlias = !empty($alias);
        $sql = Church::getChurchDetailsSql($useAlias);
        $param = $useAlias ? $alias : $id;
        $resultArray = $this->connection->fetchAllAssociative($sql, [$param], [Types::ASCII_STRING]);
        if (empty($resultArray)) {
            return null;
        }

        return Church::buildFromSqlResult($resultArray);
    }

    /**
     * @param array $churchIds
     * @return array associative array containing ChurchBasicInfo objects by id
     */
    function getChurchBasicInfo(array $churchIds): array
    {
        $sql = "
        select 
            kostol_info.i_id_item,
            kostol_info.i_id_param,
            kostol_info.t_value
        from catalog_item_param_value as kostol_info
        where kostol_info.i_id_item in (?) 
            and kostol_info.i_id_param in (4002, 4003, 4004, 4101, 4102, 4201, 4202)
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        return ChurchBasicInfo::buildFromSqlResult($resultArray);
    }

    /**
     * @return array associative array containing ChurchArrayItem objects by id
     */
    function getAllChurches(): array
    {
        $sql = "
        select 
            kostol.i_id_item, 
            kostol_info.i_id_param, 
            kostol.t_title, 
            kostol.t_alias, 
            kostol_info.t_value
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol.s_pub = '1' 
            and kostol_info.i_id_param in (4003, 4004)
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);

        $churches = [];
        foreach ($resultArray as $result) {
            $id = intval($result['i_id_item']);
            $church = isset($churches[$id]) ? $churches[$id] : new stdClass();
            $church->id = $id;
            $church->title = $result['t_title'];
            $church->alias = $result['t_alias'];

            switch ($result['i_id_param']) {
                case ChurchParams::Town:
                    $church->townId = intval($result['t_value']);
                    break;
                case ChurchParams::Parish:
                    $church->parishId = intval($result['t_value']);
                    break;
            }
            $churches[$id] = $church;
        }

        return $churches;
    }

    /**
     * @return array associative array containing church gps coordinates by id
     */
    function getAllChurchesGps(): array
    {
        $sql = "
        select 
            kostol.i_id_item, 
            kostol_info.t_value
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol.s_pub = '1' 
            and kostol_info.i_id_param = 4008
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);

        $churches = [];
        foreach ($resultArray as $result) {
            $churches[$result['i_id_item']] = $result['t_value'];
        }

        return $churches;
    }

    /**
     * @return array associative array containing town id by church id
     */
    function getAllChurchesTowns(): array
    {
        $sql = "
        select
            kostol.i_id_item,  
            kostol_info.t_value
        from catalog_item as kostol
            join catalog_item_param_value as kostol_info on kostol_info.i_id_item = kostol.i_id_item
        where kostol.i_id_catalog = 4 
            and kostol.s_pub = '1'    
            and kostol_info.i_id_param = 4004 
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);

        $churches = [];
        foreach ($resultArray as $result) {
            $churches[$result['i_id_item']] = $result['t_value'];
        }

        return $churches;
    }

    /**
     * @param string $id church id
     * @return string church announcements URL
     */
    function getChurchAnnouncements(string $id): ?string
    {
        $sql = "
        select 
            kostol_info.t_value
        from catalog_item_param_value as kostol_info
        join catalog_item as kostol on kostol.i_id_item = kostol_info.i_id_item
        where kostol.i_id_item = ?
            and kostol_info.i_id_param = 4015
            and kostol.i_id_catalog = 4 
            and kostol.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        if (!$result || empty($result['t_value'])) {
            return null;
        }
        
        return $result['t_value'];
    }

}
