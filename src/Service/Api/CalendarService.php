<?php

namespace App\Service\Api;

use App\Entity\Api\DateSettings;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;

class CalendarService
{
    private $connection;

    const MASS_SPECIAL_DATES = [
        "2023-04-06","2023-04-07","2023-04-08","2023-04-10","2023-07-05","2023-09-15","2023-12-24","2023-12-26","2023-12-31",
        "2024-03-28","2024-03-29","2024-03-30","2024-04-01","2024-06-29","2024-07-05","2024-12-24","2024-12-26","2024-12-31"
    ];

    const CONFESSION_SPECIAL_DATES = [
        "2023-04-06","2023-04-07","2023-04-08","2023-04-10","2023-07-05","2023-09-15","2023-12-24","2023-12-26","2023-12-31",
        "2024-03-28","2024-03-29","2024-03-30","2024-04-01","2024-06-29","2024-07-05","2024-12-24","2024-12-26","2024-12-31"
    ];


    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getSpecialDates()
    {
        return [
            'mass' => self::MASS_SPECIAL_DATES,
            'confession' => self::CONFESSION_SPECIAL_DATES
        ];
    }

    public static function isMassSpecialDate($date)
    {
        return in_array($date, self::MASS_SPECIAL_DATES);
    }

    public static function isConfessionSpecialDate($date)
    {
        return in_array($date, self::CONFESSION_SPECIAL_DATES);
    }

    public static function isInSeason($date, $dateFrom, $dateTo)
    {
        return $date >= $dateFrom && $date <= $dateTo;
    }

    function getDateSettings(string $date)
    {
        $sql = "
        select 
            d_date as date,
            t_spomienka as celebration,
            t_sviatok as celebrationRank,
            s_statny_sviatok as isStateHoliday,
            s_prikazany_sviatok as isObligatoryFeast,   
            s_zvykovy_sviatok as isHabitualFeast
        from kalendar 
        where d_date=?
        ";
        $result = $this->connection->fetchAssociative($sql, [$date], [Types::ASCII_STRING]);

        return DateSettings::buildFromSqlResult($result);
    }

    function getHolyDays(string $date)
    {
        $sql = "
        select 
            d_date,
            t_spomienka 
        from kalendar 
        where d_date >= ? 
        and s_notifikacia = '1'
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$date], [Types::ASCII_STRING]);

        $holyDays = [];
        foreach ($resultArray as $result) {
            $holyDays[$result['d_date']] = $result['t_spomienka'];
        }

        return $holyDays;
    }

}
