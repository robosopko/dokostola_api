<?php

namespace App\Service\Api;

use App\Entity\Api\ChurchParams;
use App\Entity\Api\Deanery;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;
use stdClass;

class DeaneryService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    function getDeaneryByAlias(string $alias): Deanery
    {
        $sql = "
        select 
            dekanat.i_id_item as id, 
            dekanat.t_title as title, 
            dekanat.t_alias as alias 
        from catalog_item as dekanat
        where dekanat.t_alias = ? 
            and dekanat.i_id_catalog = 2 
            and dekanat.i_id_category = 2 
            and dekanat.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$alias], [Types::ASCII_STRING]);

        return Deanery::buildBasicFromSqlResult($result);
    }

    function getDeaneryById(string $id): Deanery
    {
        $sql = "
        select 
            dekanat.i_id_item as id, 
            dekanat.t_title as title, 
            dekanat.t_alias as alias 
        from catalog_item as dekanat
        where dekanat.i_id_item = ? 
            and dekanat.i_id_catalog = 2 
            and dekanat.i_id_category = 2 
            and dekanat.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        return Deanery::buildBasicFromSqlResult($result);
    }

    function getDeaneryInfo(?string $id, ?string $alias): ?Deanery
    {
        $useAlias = !empty($alias);
        $sql = Deanery::getDeaneryDetailsSql($useAlias);
        $param = $useAlias ? $alias : $id;
        $resultArray = $this->connection->fetchAllAssociative($sql, [$param], [Types::ASCII_STRING]);
        if (empty($resultArray)) {
            return null;
        }

        return Deanery::buildExtendedFromSqlResult($resultArray);
    }

    /**
     * @param array $dioceseIds
     * @return array associative array containing Deanery objects by id
     */
    function getDeaneriesInDiocese(array $dioceseIds): array
    {
        $sql = "
        select 
            deanery.i_id_item as id, 
            deanery.t_title as title, 
            deanery.t_alias as alias,
            deanery_info.t_value as dioceseId
        from catalog_item as deanery
        join catalog_item_param_value as deanery_info on deanery_info.i_id_item = deanery.i_id_item and deanery_info.i_id_param = 2201
        where deanery.i_id_catalog = 2 
            and deanery.i_id_category = 2 
            and deanery_info.t_value in (?) 
            and deanery.s_pub = '1'
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$dioceseIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $deaneries = [];
        foreach ($resultArray as $result) {
            $deaneries[$result['id']] = Deanery::buildBasicFromSqlResult($result);
        }

        return $deaneries;
    }

}
