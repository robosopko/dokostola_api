<?php

namespace App\Service\Api;

use App\Entity\Api\Confession;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;

class ConfessionService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $churchIds
     * @param int $day
     * @param bool $isSeason
     * @return array associative array of Confession arrays by church id
     */
    function getRegularConfessions(array $churchIds, int $day, bool $isSeason): array
    {
        $sql = "
        select 
            i_id_sp as id, 
            i_id_kostol as churchId, 
            i_id_den as day, 
            d_time as time, 
            d_etime as endTime, 
            i_id_jazyk as languageId, 
            t_note as note, 
            s_sezonne as isSeasonal
        from spovedania 
        where i_id_kostol in (?) 
            and i_id_den = ? 
            and s_sezonne = ? 
        order by d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchIds, $day, $isSeason ? '1' : '0'], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY, Types::ASCII_STRING, Types::ASCII_STRING]);

        $confessions = [];
        foreach ($resultArray as $result) {
            $confessions[$result['churchId']][] = Confession::buildFromSqlResult($result);
        }
        return $confessions;
    }

    /**
     * @param array $churchIds
     * @param string $date
     * @return array associative array of Confession arrays by churchId
     */
    function getExtraordinaryConfessions(array $churchIds, string $date): array
    {
        $sql = "
        select 
            i_id_sp as id, 
            i_id_kostol as churchId, 
            d_date as date, 
            d_time as time,
            d_etime as endTime, 
            i_id_jazyk as languageId,  
            t_note as note, 
            s_sezonne as isSeasonal, 
            s_iba as overrideRegular, 
            s_bez as noConfessionToday
        from spovedania 
        where i_id_kostol in (?) 
            and d_date = ?
        order by d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchIds, $date], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY, Types::ASCII_STRING]);

        $confessions = [];
        foreach ($resultArray as $result) {
            $confessions[$result['churchId']][] = Confession::buildFromSqlResult($result);
        }
        return $confessions;
    }

    /**
     * @param string $churchId
     * @param bool $isSeason
     * @return Confession[]
     */
    function getChurchRegularConfessionsByIdAndSeason(string $churchId, bool $isSeason): array
    {
        $sql = "
        select 
            i_id_sp as id, 
            i_id_kostol as churchId, 
            i_id_den as day, 
            d_time as time, 
            d_etime as endTime, 
            i_id_jazyk as languageId, 
            t_note as note, 
            s_sezonne as isSeasonal
        from spovedania 
        where i_id_kostol in (?) 
            and i_id_den > 0 
            and s_sezonne = ?
        order by i_id_den, d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchId, $isSeason ? '1' : '0'], [Types::ASCII_STRING, Types::ASCII_STRING]);

        $confessions = [];
        foreach ($resultArray as $result) {
            $confessions[] = Confession::buildFromSqlResult($result);
        }
        return $confessions;
    }

    /**
     * @param string $churchId
     * @return Confession[]
     */
    function getChurchRegularConfessions(string $churchId): array
    {
        return $this->getChurchRegularConfessionsByIdAndSeason($churchId, false);
    }

    /**
     * @param string $churchId
     * @return Confession[]
     */
    function getChurchSeasonalConfessions(string $churchId): array
    {
        return $this->getChurchRegularConfessionsByIdAndSeason($churchId, true);
    }

    /**
     * @param string $churchId
     * @return Confession[]
     */
    function getChurchExtraordinaryConfessions(string $churchId): array
    {
        $sql = "
        select 
            i_id_sp as id, 
            i_id_kostol as churchId, 
            d_date as date, 
            d_time as time,
            d_etime as endTime, 
            i_id_jazyk as languageId,  
            t_note as note, 
            s_sezonne as isSeasonal, 
            s_iba as overrideRegular, 
            s_bez as noConfessionToday
        from spovedania 
        where i_id_kostol = ? 
            and d_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) 
        order by d_date, d_time
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$churchId], [Types::ASCII_STRING]);

        $confessions = [];
        foreach ($resultArray as $result) {
            $confessions[] = Confession::buildFromSqlResult($result);
        }
        return $confessions;
    }

}
