<?php

namespace App\Service\Api;

use App\Entity\Api\Town;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;

class TownService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }


    /**
     * @return array associative array of Town objects by id
     */
    function getAllTowns(): array
    {
        $sql = "
        select 
            catalog_item.i_id_item as id, 
            catalog_item.t_title as title, 
            catalog_item.t_alias as alias, 
            catalog_item_param_value.t_value as countyId
        from catalog_item
            join catalog_item_param_value on catalog_item_param_value.i_id_item = catalog_item.i_id_item and catalog_item_param_value.i_id_param = 1331
            where catalog_item.i_id_catalog = 1 
                and catalog_item.i_id_category = 3
        order by catalog_item.t_title;
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);

        $towns = [];
        $towns['1'] = Town::buildFromSqlResult([
            'id' => '1',
            'title' => 'Bratislava',
            'alias' => 'bratislava',
            'countyId' => '1'
        ]);
        $towns['2'] = Town::buildFromSqlResult([
            'id' => '2',
            'title' => 'Košice',
            'alias' => 'kosice',
            'countyId' => '2'
        ]);
        foreach ($resultArray as $result) {
            $towns[$result['id']] = Town::buildFromSqlResult($result);
        }

        return $towns;
    }

    /**
     * @return Town object
     */
    function getTownById(string $id): ?Town
    {
        if ($id == '1') {
            return Town::buildFromSqlResult([
                'id' => '1',
                'title' => 'Bratislava',
                'alias' => 'bratislava',
                'countyId' => '1'
            ]);
        }
        if ($id == '2') {
            return Town::buildFromSqlResult([
                'id' => '2',
                'title' => 'Košice',
                'alias' => 'kosice',
                'countyId' => '2'
            ]);
        }

        $sql = "
        select 
            catalog_item.i_id_item as id, 
            catalog_item.t_title as title, 
            catalog_item.t_alias as alias, 
            catalog_item_param_value.t_value as countyId
        from catalog_item
            join catalog_item_param_value on catalog_item_param_value.i_id_item = catalog_item.i_id_item and catalog_item_param_value.i_id_param = 1331
            where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 3
            and catalog_item.i_id_item = ?;
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        if (!$result) {
            return null;
        }

        return Town::buildFromSqlResult($result);
    }

    /**
     * @return Town[] array of counties
     */
    function getCountiesByIds(array $countyIds): array
    {
        $sql = "
        select
            catalog_item.i_id_item as id, 
            catalog_item.t_title as title, 
            catalog_item.t_alias as alias,
            0 as countyId
        from catalog_item
        where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 2
            and catalog_item.i_id_item in (?);
        ";

        $resultArray = $this->connection->fetchAllAssociative($sql, [$countyIds], [\Doctrine\DBAL\Connection::PARAM_STR_ARRAY]);

        $counties = [];

        if (in_array('1', $countyIds)) {
            $counties['1'] = Town::buildFromSqlResult([
                'id' => '1',
                'title' => 'Bratislava',
                'alias' => 'bratislava',
                'countyId' => '1'
            ]);
        }

        if (in_array('2', $countyIds)) {
            $counties['2'] = Town::buildFromSqlResult([
                'id' => '2',
                'title' => 'Košice',
                'alias' => 'kosice',
                'countyId' => '2'
            ]);
        }

        foreach ($resultArray as $result) {
            $counties[$result['id']] = Town::buildFromSqlResult($result);
        }

        return $counties;
    }

    /**
     * @return Town[] array of towns
     */
    function getTownsByIds(array $townIds, bool $expand): array
    {
        if (count($townIds) > 0 && !is_numeric($townIds[0])) {
            $townIds = $this->getTownIdsByAliases($townIds);
        }

        if ($expand) {
            $townIds = $this->expandTownIds($townIds);
        }

        $countyIds = $expand ? [] : array_filter($townIds, function ($townId) {
            return $townId < 3000;
        });

        $counties = [];

        if (!empty($countyIds)) {
            $counties = $this->getCountiesByIds($countyIds);
        }

        $townIds = array_filter($townIds, function ($townId) {
            return $townId > 3000;
        });

        $towns = [];

        $sql = "
        select 
            catalog_item.i_id_item as id, 
            catalog_item.t_title as title, 
            catalog_item.t_alias as alias, 
            catalog_item_param_value.t_value as countyId
        from catalog_item
            join catalog_item_param_value on catalog_item_param_value.i_id_item = catalog_item.i_id_item and catalog_item_param_value.i_id_param = 1331
            where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 3
            and catalog_item.i_id_item in (?);
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$townIds], [\Doctrine\DBAL\Connection::PARAM_STR_ARRAY]);

        foreach ($resultArray as $result) {
            $towns[$result['id']] = Town::buildFromSqlResult($result);
        }

        return $counties + $towns;
    }

    /**
     * @return string[] array of townIds
     */
    function getTownIdsByAliases(array $aliases): array
    {
        $countyAliases = array_map(function($alias) { return substr($alias, 6); },
            array_filter($aliases,
            function($alias) {
                return strncmp($alias, "okres-", 6) === 0;
            })
        );

        $sql = "
        select
            catalog_item.i_id_item as id
        from catalog_item
        where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 2
            and catalog_item.t_alias in (?); 
        ";
        $townIds = $this->connection->fetchFirstColumn($sql, [$countyAliases], [\Doctrine\DBAL\Connection::PARAM_STR_ARRAY]);

        $townAliases = array_filter($aliases,
            function($alias) {
                return strncmp($alias, "okres-", 6) !== 0;
            }
        );

        $sql = "
        select 
            catalog_item.i_id_item as id 
        from catalog_item
            where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 3
            and catalog_item.t_alias in (?);
        ";
        $resultArray = $this->connection->fetchFirstColumn($sql, [$townAliases], [\Doctrine\DBAL\Connection::PARAM_STR_ARRAY]);

        $townIds = array_merge($townIds, $resultArray);

        if (in_array('bratislava', $aliases)) {
            $townIds[] = '1';
        }
        if (in_array('kosice', $aliases)) {
            $townIds[] = '2';
        }

        return $townIds;
    }

    /**
     * @return array associative array of counties (Town objects) by id
     */
    function getAllCounties(): array
    {
        $sql = "
        select
            catalog_item.i_id_item as id, 
            catalog_item.t_title as title, 
            catalog_item.t_alias as alias,
            0 as countyId
        from catalog_item
        where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 2
            and catalog_item.i_id_item not in (2080, 412975, 414091)
            and catalog_item.s_pub = '1'; 
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);
        $counties = [];

        foreach ($resultArray as $result) {
            $counties[$result['id']] = Town::buildFromSqlResult($result);
        }

        return $counties;
    }

    /**
     * @return array array of town ids for given county
     */
    function getTownIdsForCounty(string $countyId): array
    {
        $sql = "
        select 
            catalog_item.i_id_item as id 
        from catalog_item join catalog_item_param_value
            on catalog_item_param_value.i_id_item = catalog_item.i_id_item 
        where catalog_item.i_id_catalog = 1 
            and catalog_item.i_id_category = 3 
            and catalog_item_param_value.t_value = ?
            and catalog_item.s_pub = '1'; 
        ";
        $resultArray = $this->connection->fetchFirstColumn($sql, [$countyId], [Types::ASCII_STRING]);

        return $resultArray;
    }

    function expandTownIds(array $townIds): array
    {
        $bratislavaIds = ['30001','30002','30003','30004','30005','30006','30007','30008','30009','30010','30011','30012','30013','30014','30015','30016','30017'];
        $kosiceIds = ['32488','32489','32490','32491','32492','32493','32494','32495','32496','32497','32498','32499','32500','32501','32502','32503','32504','32505','32506','32507','32508','32509'];

        $expandedTownIds = [];
        foreach ($townIds as $townId) {
            if ($townId == '1') {
                $expandedTownIds = array_merge($expandedTownIds, $bratislavaIds);
            } elseif ($townId == '2') {
                $expandedTownIds = array_merge($expandedTownIds, $kosiceIds);
            } elseif ($townId < 3000) {
                $expandedTownIds = array_merge($expandedTownIds, $this->getTownIdsForCounty($townId));
            } else {
                $expandedTownIds[] = $townId;
            }
        }

        return $expandedTownIds;
    }
}
