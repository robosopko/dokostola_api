<?php

namespace App\Service\Api;

use App\Entity\Api\ChurchParams;
use App\Entity\Api\Parish;
use App\Entity\Api\ParishArrayItem;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Types\Types;
use stdClass;

class ParishService
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    function getParishByAlias(string $alias): ParishArrayItem
    {
        $sql = "
        select 
            farnost.i_id_item as id, 
            farnost.t_title as title, 
            farnost.t_alias as alias,
            farnost_info.t_value as deaneryId
        from catalog_item as farnost
        join catalog_item_param_value as farnost_info on farnost_info.i_id_item = farnost.i_id_item and farnost_info.i_id_param = 3002
        where farnost.t_alias = ? 
            and farnost.i_id_catalog = 3 
            and farnost.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$alias], [Types::ASCII_STRING]);

        return ParishArrayItem::buildFromSqlResult($result);
    }

    function getParishById(string $id): ParishArrayItem
    {
        $sql = "
        select 
            farnost.i_id_item as id, 
            farnost.t_title as title, 
            farnost.t_alias as alias,
            farnost_info.t_value as deaneryId
        from catalog_item as farnost
        join catalog_item_param_value as farnost_info on farnost_info.i_id_item = farnost.i_id_item and farnost_info.i_id_param = 3002
        where farnost.i_id_item = ? 
            and farnost.i_id_catalog = 3 
            and farnost.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        return ParishArrayItem::buildFromSqlResult($result);
    }

    function getParishInfo(?string $id, ?string $alias): ?Parish
    {
        $useAlias = !empty($alias);
        $sql = Parish::getParishDetailsSql($useAlias);
        $param = $useAlias ? $alias : $id;
        $resultArray = $this->connection->fetchAllAssociative($sql, [$param], [Types::ASCII_STRING]);
        if (empty($resultArray)) {
            return null;
        }

        return Parish::buildFromSqlResult($resultArray);
    }

    /**
     * @param array $deaneryIds
     * @return array associative array containing ParishArrayItem objects by id
     */
    function getParishesInDeanery(array $deaneryIds): array
    {
        $sql = "
        select 
            parish.i_id_item as id, 
            parish.t_title as title, 
            parish.t_alias as alias, 
            parish_info.t_value as deaneryId
        from catalog_item as parish
        join catalog_item_param_value as parish_info on parish_info.i_id_item = parish.i_id_item and parish_info.i_id_param = 3002
        where parish.i_id_catalog = 3 
            and parish_info.t_value in (?) 
            and parish.s_pub = '1'
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql, [$deaneryIds], [\Doctrine\DBAL\Connection::PARAM_INT_ARRAY]);

        $parishes = [];
        foreach ($resultArray as $result) {
            $parishes[$result['id']] =  ParishArrayItem::buildFromSqlResult($result);
        }

        return $parishes;
    }

    /**
     * @return array associative array containing ParishArrayItem objects by id
     */
    function getAllParishes(): array
    {
        $sql = "
        select 
            parish.i_id_item as id, 
            parish.t_title as title, 
            parish.t_alias as alias, 
            parish_info.t_value as deaneryId
        from catalog_item as parish
        join catalog_item_param_value as parish_info on parish_info.i_id_item = parish.i_id_item and parish_info.i_id_param = 3002
        where parish.i_id_catalog = 3 
            and parish.s_pub = '1'
        ";
        $resultArray = $this->connection->fetchAllAssociative($sql);

        $parishes = [];
        foreach ($resultArray as $result) {
            $parishes[$result['id']] =  ParishArrayItem::buildFromSqlResult($result);
        }

        return $parishes;
    }

    /**
     * @param string $id parish id
     * @return string parish announcements URL
     */
    function getParishAnnouncements(string $id): ?string
    {
        $sql = "
        select 
            farnost_info.t_value
        from catalog_item_param_value as farnost_info
        join catalog_item as farnost on farnost.i_id_item = farnost_info.i_id_item
        where farnost.i_id_item = ? 
            and farnost_info.i_id_param = 3019
            and farnost.i_id_catalog = 3 
            and farnost.s_pub = '1'
        ";
        $result = $this->connection->fetchAssociative($sql, [$id], [Types::ASCII_STRING]);

        if (!$result || empty($result['t_value'])) {
            return null;
        }
        
        return $result['t_value'];
    }

}
